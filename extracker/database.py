from mongoengine import *
from flask_mongoengine import MongoEngine
from bson import ObjectId
from flask.ext.security import UserMixin, RoleMixin

db = MongoEngine()

### Documents
class Field(db.EmbeddedDocument):
    id = ObjectIdField(unique=True, required=True, default=ObjectId)
    fieldname = StringField(unique=True)
    fieldfield = StringField()
    fieldtype = StringField()
    fieldrequired = BooleanField()
    placement = IntField()

class TypeDocument(db.Document):
    documenttype = StringField(required=True)
    typename = StringField(required=True, unique=True)
    owner = StringField()
    description = StringField()
    fields = EmbeddedDocumentListField(Field)

class TypeEntry(db.DynamicDocument):
    documenttype = ReferenceField(TypeDocument)

## Flask-Security Models

class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)

class User(db.Document, UserMixin):
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])
