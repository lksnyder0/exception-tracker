import json
import dateutil.parser

from extracker import app, user_datastore
from flask_assets import Bundle
from extracker.database import *
from extracker.forms import *
from flask import render_template, redirect, flash, url_for, request
from wtforms import TextField
from flask_security import login_required, roles_required, roles_accepted
from flask_security.utils import encrypt_password

@app.route('/')
def index():
    return redirect(url_for('showTypes', typename="exception"))

@app.route('/<typename>/<id>')
@login_required
@roles_accepted('admin', 'reader')
def showTypeEntries(typename, id):
    documentType = TypeDocument.objects.get_or_404(id=id)
    return render_template('entryview.html', documentType=documentType)

@app.route('/<typename>/<id>/get')
@login_required
@roles_accepted('admin', 'reader')
def getEntries(typename, id):
    response = dict(data=[], headers=[])
    documentType = TypeDocument.objects.get_or_404(id=id)
    entries = TypeEntry.objects(documenttype=documentType).exclude("documenttype").exclude("typename")
    for entry in entries:
        dictEntry = json.loads(entry.to_json())
        dictEntry["controls"] = render_template("controls.html", typename=typename, id=str(entry.id))
        response["data"].append(dictEntry)
    sortedFields = sorted(documentType.fields, key=lambda field: field.placement)
    for field in sortedFields:
        response["headers"].append(dict(title=field.fieldname, data=field.fieldfield))
    response["headers"].append(dict(title="", data="controls"))
    return json.dumps(response)

@app.route('/<typename>/<id>/new', methods=["GET", "POST"])
@login_required
@roles_required('admin')
def newEntry(typename, id):
    documentType = TypeDocument.objects.get_or_404(id=id)
    sortedFields = sorted(documentType.fields, key=lambda field: field.placement)
    newEntryForm = genEntryForm(sortedFields)
    if newEntryForm.validate_on_submit():
        data = {"documenttype": documentType}
        for field in documentType.fields:
            if field.fieldtype == "date":
                data[field.fieldfield] = getattr(newEntryForm, field.fieldfield).data.isoformat()
            else:
                data[field.fieldfield] = getattr(newEntryForm, field.fieldfield).data
        newex = TypeEntry(**data).save()
        flash("Success", "success")
        return redirect(url_for('showTypeEntries', typename=typename, id=id))
    elif request.method == "POST":
        for field in newEntryForm:
            for error in field.errors:
                flash(error, "danger")
        return redirect("/ex/" + id)
    title = "Add new %s exception" % (documentType.typename)
    return render_template('neweditentrymodal.html', title=title, form=newEntryForm, action=url_for('newEntry', typename=typename, id=str(documentType.id)))

@app.route('/<typename>/<id>/edit', methods=["GET", "POST"])
@login_required
@roles_required('admin')
def editEntry(typename, id):
    entry = TypeEntry.objects.get_or_404(id=id)
    documentType = entry.documenttype
    sortedFields = sorted(documentType.fields, key=lambda field: field.placement)
    editEntryForm = genEntryForm(sortedFields)
    if editEntryForm.validate_on_submit():
        data = {}
        for field in editEntryForm:
            for typefield in documentType.fields:
                if typefield.fieldtype == "date":
                    data[typefield.fieldfield] = getattr(editEntryForm, typefield.fieldfield).data.isoformat()
                else:
                    data[typefield.fieldfield] = getattr(editEntryForm, typefield.fieldfield).data
            entry.update(**data)
            flash("Exception successfully updated", "success")
            return redirect(url_for('showTypeEntries', typename=typename, id=documentType.id))
    elif request.method == "POST":
        for field in editEntryForm:
            for error in field.errors:
                flash(field.label.text + ": " + error, "danger")
    data = json.loads(entry.to_json())
    for typefield in documentType.fields:
        if typefield.fieldtype == "date":
            getattr(editEntryForm, typefield.fieldfield).data = dateutil.parser.parse(data[typefield.fieldfield])
        else:
            getattr(editEntryForm, typefield.fieldfield).data = data[typefield.fieldfield]
    title = "Edit %s exception" % (documentType.typename)
    return render_template('neweditentrymodal.html', title=title, form=editEntryForm, action=url_for('editEntry', typename=typename, id=str(entry.id)))

@app.route('/<typename>/<id>/delete')
@login_required
@roles_required('admin')
def deleteEntry(typename, id):
    entry = TypeEntry.objects.get_or_404(id=id)
    typeid = entry.documenttype.id
    try:
        entry.delete()
    except Exception as e:
        flash("Failed to delete exception", "danger")
        flash(e, "danger")
    else:
        flash("Successfully deleted exception", "success")
    return redirect(url_for('showTypeEntries', typename=typename, id=typeid))

## Management Routes

@app.route('/manage/<typename>')
@login_required
@roles_accepted('admin', 'reader')
def showTypes(typename):
    if typename in ['exception', 'object']:
        counts = {}
        types = TypeDocument.objects(documenttype=typename)
        for otype in types:
            counts[otype.id] = TypeEntry.objects(documenttype=otype).count()
        return render_template('typeview.html', typename=typename, types=types, typecounts=counts)

@app.route('/manage/<typename>/new', methods=["GET", "POST"])
@login_required
@roles_required('admin')
def newTypeDefinition(typename):
    if typename in ['exception', 'object']:
        typeForm = TypeForm()
        if typeForm.validate_on_submit():
            name = typeForm.typename.data
            if typeForm.owner.data == "":
                owner = "N/A"
            else:
                owner = typeForm.owner.data
            description = typeForm.description.data
            try:
                newTypeDocument = TypeDocument(documenttype=typename, typename=name, owner=owner, description=description).save()
            except NotUniqueError:
                typeForm.typename.errors.append("Name must be unique")
            else:
                flash("%s successfully added to %ss" % (name, typename), "success")
                return redirect(url_for('editType', typename=typename, id=newTypeDocument.id))
        elif request.method == 'POST':
            flash("Form Validation Failed", "danger")
        return render_template(
                'newedittype.html',
                typename=typename,
                typeForm=typeForm,
                sectiontitle="New " + typename + " type",
                submiturl=url_for('newTypeDefinition', typename=typename)
        )

@app.route('/manage/<typename>/<id>/edit', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def editType(typename, id):
    if typename in ['exception', 'object']:
        typeDocument = TypeDocument.objects(id=id).first()
        typeForm = TypeForm()
        if typeForm.validate_on_submit():
            try:
                typeDocument.update(
                        typename=typeForm.typename.data,
                        owner=typeForm.owner.data,
                        description=typeForm.description.data
                        )
            except NotUniqueError:
                typeForm.typename.errors.append("Name must be unique")
            else:
                flash("Exception type successfully updated.", "success")
                return redirect(url_for('showTypes', typename="exception"))
        typeForm.typename.data = typeDocument.typename
        typeForm.owner.data = typeDocument.owner
        typeForm.description.data = typeDocument.description
        fields = []
        for field in typeDocument.fields:
            fieldForm = TypeFieldForm()
            fieldForm.fieldname.data = field.fieldname
            fieldForm.fieldtype.data = field.fieldtype
            fieldForm.fieldrequired.data = field.fieldrequired
            fieldForm.typeid.data = id
            fieldForm.fieldid.data = field.id
            fieldForm.placement.data = field.placement
            fields.append(fieldForm)
        sortedFields = sorted(fields, key=lambda fieldForm: fieldForm.placement.data)
        sectiontitle = "Edit %s" % (typeDocument.typename)
        return render_template(
                "newedittype.html",
                typename=typename,
                typeForm=typeForm, 
                sectiontitle=sectiontitle, 
                submiturl=url_for('editType', typename=typeDocument.documenttype, id=typeDocument.id), 
                fields=sortedFields,
                typeDocumentID=id
        )

@app.route('/manage/<typename>/<id>/delete')
@login_required
@roles_required('admin')
def deleteType(typename, id):
    if typename in ['exception', 'object']:
        typeDocument = TypeDocument.objects.get(documenttype=typename, id=id)
        name = typeDocument.typename
        try:
            typeDocument.delete()
        except Exception as e:
            flash("Failed to delete %s from %s" % (typeDocument.typename, typename), "danger")
            flash(e, "danger")
        else:
            flash("Successfully deleted %s from %s" % (name, typename), "success")
        return redirect(url_for("showTypes", typename=typename))

@app.route('/manage/fields/new/<id>', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def newExceptionTypeField(id):
    fieldForm = TypeFieldForm()
    if request.method == "GET":
        fieldForm.typeid.data = id
        return render_template('newtypefieldform.html', fieldForm=fieldForm)
    else:
        if fieldForm.validate_on_submit():
            documentType = TypeDocument.objects(id=id).first()
            newfield = Field(
                    fieldname=fieldForm.fieldname.data,
                    fieldfield=''.join(e for e in fieldForm.fieldname.data if e.isalnum()).lower(),
                    fieldrequired=fieldForm.fieldrequired.data,
                    fieldtype=fieldForm.fieldtype.data,
                    placement=fieldForm.placement.data)
            documentType.update(add_to_set__fields=newfield)
            documentType.cascade_save()
            ## Need to add new field to existing exceptions
            nf = {newfield.fieldfield:""}
            entrys = TypeEntry.objects(documenttype=documentType)
            for entry in entrys:
                entry.update(**nf)
            return json.dumps(dict(status="success", id=str(newfield.id)))
        else:
            status = {"status": "error", "messages": []}
            for field in fieldForm:
                for error in field.errors:
                    status["messages"].append({"name":field.name, "error": error})
        return json.dumps(status)


@app.route('/manage/fields/edit', methods=['POST'])
@login_required
@roles_required('admin')
def editTypeField():
    fieldForm = TypeFieldForm()
    response = {}
    if fieldForm.validate_on_submit():
        documentType = TypeDocument.objects.get(id=fieldForm.typeid.data)
        field = documentType.fields.filter(id=fieldForm.fieldid.data).first()
        origionalName = field.fieldfield
        field.fieldname = fieldForm.fieldname.data
        field.fieldfield = ''.join(e for e in fieldForm.fieldname.data if e.isalnum()).lower()
        field.fieldtype = fieldForm.fieldtype.data
        field.fieldrequired = fieldForm.fieldrequired.data
        field.placement = fieldForm.placement.data
        try:
            field.save()
        except Exception as e:
            response["status"] = "error"
            response["messages"] = [e]
        else:
            if origionalName != field.fieldfield:
                query = {"$rename":{origionalName:field.fieldfield}}
                entries = TypeEntry.objects(documenttype=documentType).update(__raw__=query)
            response["status"] = "success"
            response["messages"] = []
        return json.dumps(response)
    else:
        response["status"] = "error"
        response["messages"] = []
        for field in fieldForm:
            for error in field.errors:
                response["messages"].append({"name":field.name, "error": error})
        return json.dumps(response)

@app.route('/manage/<typename>/<typeid>/field/<fieldid>/delete')
@login_required
@roles_required('admin')
def deleteExceptionTypeField(typename, typeid, fieldid):
    try:
        documentType = TypeDocument.objects.get_or_404(id=typeid)
        fieldname = documentType.fields.filter(id=fieldid).first().fieldfield
        entries = TypeEntry.objects(documenttype=documentType).update(**{"unset__"+fieldname:1})
        documentType.update(pull__fields__id=fieldid)
    except Exception as e:
        flash("Failed to delete field", "danger")
        flash(e, "danger")
    else:
        flash("Field successfully deleted", "success")
    return redirect(url_for('editType', typename=typename, id=typeid,))

@app.route('/users', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def users():
    roles = [(str(role.id), role.name) for role in Role.objects.all()]
    newUser = NewUserForm()
    newUser.roles.choices = roles
    deleteUser = DeleteUserForm()
    editUser = EditUserForm()
    editUser.roles.choices = roles
    users = User.objects.all()
    return render_template("users.html", newUserForm=newUser, deleteUserForm=deleteUser, editUserForm=editUser, users=users)

@app.route('/users/add', methods=['POST'])
@login_required
@roles_required('admin')
def addUser():
    newUser = NewUserForm()
    newUser.roles.choices = [(str(role.id), role.name) for role in Role.objects.all()]
    if newUser.validate_on_submit():
        flash("User successfully added", "success")
        password = encrypt_password(newUser.password.data)
        user_datastore.create_user(email=newUser.email.data, password=password)
        for role in newUser.roles.data:
            roleObject = Role.objects.get(id=role)
            user_datastore.add_role_to_user(newUser.email.data, roleObject.name)
        return redirect(url_for('users'))
    elif request.method == 'POST':
        flash("Failed to add user", 'danger')
    return redirect(url_for('users'))

@app.route('/users/delete', methods=['POST'])
@login_required
@roles_required('admin')
def deleteUser():
    deleteUser = DeleteUserForm()
    if deleteUser.validate_on_submit():
        User.objects.get_or_404(id=deleteUser.user_id.data).delete()
        flash("Successfully deleted user", "success")
    else:
        flash("Deleting user failed", "danger")
    return redirect(url_for('users'))

@app.route('/users/<id>', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def editUser(id):
    if request.method == "GET":
        user = User.objects.get_or_404(id=id)
        roles = [ str(role.id) for role in user.roles ]
        return json.dumps(dict(email=user.email, roles=roles))
    else:
        rdata = dict()
        roles = [(str(role.id), role.name) for role in Role.objects.all()]
        editUser = EditUserForm()
        editUser.roles.choices = roles
        if editUser.validate_on_submit():
            user = User.objects.get(id=editUser.user_id.data)
            if len(roles) > 0:
                requestedRoles = []
                for roleID in editUser.roles.data:
                    requestedRoles.append(Role.objects.get(id=roleID).name)
                currentRoles = [ role.name for role in user_datastore.get_user(editUser.user_id.data).roles ]
                for role in currentRoles:
                    if role not in requestedRoles:
                        user_datastore.remove_role_from_user(user.email, role)
                for role in requestedRoles:
                    if role not in currentRoles:
                        user_datastore.add_role_to_user(user.email, role)
            if editUser.new_password.data != "":
                password = encrypt_password(editUser.new_password.data)
                user.update(password=password)
            rdata["status"] = "success"
            rdata["goto"] = url_for('users')
        else:
            rdata["status"] = "failure"
            rdata["messages"] = []
            for field in editUser:
                for error in field.errors:
                    rdata["messages"].append({"name":field.name, "error": error})
        return json.dumps(rdata)
    
