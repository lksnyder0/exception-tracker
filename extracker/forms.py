from flask_wtf import Form
from wtforms import TextField, StringField, TextAreaField, BooleanField, \
        HiddenField, PasswordField, SelectMultipleField, SelectField, \
        IntegerField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Optional, Email, EqualTo, Length

def genEntryForm(fieldlist):
    class DynamicForm(Form): pass
    for field in fieldlist:
        setattr(DynamicForm, field.fieldfield, __getFieldType(field))
    return DynamicForm()

def __getFieldType(field):
    if field.fieldtype == "text":
        return TextField(field.fieldname)
    elif field.fieldtype == "textarea":
        return TextAreaField(field.fieldname)
    elif field.fieldtype == "date":
        return DateField(field.fieldname, format='%Y-%m-%d')
    elif field.fieldType == "object":
        return SelectField(field.fieldname)


class TypeForm(Form):
    typename = StringField("Name", validators=[DataRequired()])
    owner = StringField("Owner", validators=[Optional()])
    description = TextAreaField("Description", validators=[Optional()])

class TypeFieldForm(Form):
    fieldname = StringField("Field Name", validators=[DataRequired()])
    fieldtype = SelectField(
            "Type",
            choices=[
                ("text", "Text Field"),
                ("textarea", "Text Area"),
                ("date", "Date"),
                ("object", "Objects")
                ],
            validators=[DataRequired()], 
            default="String")
    fieldrequired = BooleanField("Required", validators=[Optional()], default=False)
    typeid = HiddenField("exceptiontypeid", validators=[DataRequired()])
    fieldid = HiddenField("fieldid", validators=[Optional()])
    placement = HiddenField('placement', validators=[DataRequired()])

class NewUserForm(Form):
    email = StringField("Email Address", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[
        DataRequired(),
        Length(min=6, message="Password must be at least six characters"), 
        EqualTo('password_confirm', message="Passwords must match")
        ])
    password_confirm = PasswordField("Password Confirm")
    roles = SelectMultipleField("Role")

class DeleteUserForm(Form):
    user_id = HiddenField("userid", validators=[DataRequired()])

class EditUserForm(Form):
    user_id = HiddenField("userid", validators=[DataRequired()])
    email = StringField("Email Address")
    new_password = PasswordField("New Password", validators=[
        Optional(),
        Length(min=6, message="Password must be at least six characters"), 
        EqualTo('new_password_confirm', message="Passwords must match")
        ])
    new_password_confirm = PasswordField("New Password Confirm")
    roles = SelectMultipleField("Roles")

