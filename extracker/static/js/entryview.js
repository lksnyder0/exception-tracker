$(document).ready(function() {
    $("#addentry").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var typeid = button.data('typeid');
        var typename = button.data('typename');
        var modal = $(this);
        modal.find('.modal-content').empty();
        $.ajax('/' + typename + '/' + typeid + '/new').done(function(html) {
            modal.find('.modal-content').append(html)
        });
    });
    $("#editentry").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var entryid = button.data('entryid');
        var typename = button.data('typename');
        var modal = $(this);
        modal.find('.modal-content').empty();
        $.ajax('/' + typename + '/' + entryid + '/edit').done(function(html) {
            modal.find('.modal-content').append(html)
        });
    });
});
