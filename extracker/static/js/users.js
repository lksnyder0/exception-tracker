var submitAndVerifyForm = function (events, formData, url, csrftoken) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
        }
    });
    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        dataType: 'json',
        encode: true
    }).done(function(data) {
        console.log(data)
        if (data["status"] == "success") {
            console.log(data["goto"])
            window.location = data["goto"]
        } else {
            for ( var i in data["messages"] ) {
                console.log(data["messages"][i]);
                var targetField = $(events.target).find("#" + data["messages"][i]["name"])
                targetField.closest(".field-group").addClass("has-danger")
            }
        }
    });
};

$(document).ready(function() {
    $("#deleteconfirm").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var modal = $(this);
        modal.find('#user_id').val(id);
    });
    $("#edituser").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var modal = $(this);
        var data = $.ajax({
            url: '/users/' + id,
            type: 'GET',
            dataType: 'json',
            encode: true
        }).done(function(data) {
            modal.find("input[name=email]").val(data["email"]);
            modal.find("select[name=roles]").val(data["roles"]);
            modal.find("input[name=user_id]").val(id);
        });
    });
    $("#edituser").on('hide.bs.modal', function (event) {
        $(this).find("form")[0].reset();
    });
    $("#edituserform").on('submit', function(events) {
        events.preventDefault();
        var csrftoken = $(events.target).find("input[name=csrf_token]").val();
        var id = $(events.target).find("input[name=user_id]").val();
        var formData = $(events.target).serialize()
        submitAndVerifyForm(events, formData, '/users/' + id, csrftoken);
    })
});
