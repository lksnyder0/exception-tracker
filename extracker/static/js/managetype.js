var submitAndVerifyForm = function (events, formData, url, csrftoken, newfield = false) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
        }
    });
    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        dataType: 'json',
        encode: true
    }).done(function(data) {
        if (data["status"] == "success") {
            if (newfield) {
                $(events.target).removeClass("newcustomfield");
                $(events.target).addClass("customfield");
                $(events.target).find("input[name=fieldid]").val(data["id"]);
            }
            var fieldname = $(events.target).find("input[name=fieldname]");
            fieldname.closest(".field-group").addClass("has-success");
            fieldname.tooltip({
                delay: {
                    "show":500,
                    "hide":100
                },
                placement: 'top',
                title: 'Field successfully saved'
            });
            fieldname.on('input propertychange paste', function () {
                $(this).tooltip('dispose');
                fieldname.closest(".field-group").removeClass("has-success");
            });
        } else {
            for ( var i in data["messages"] ) {
                var targetField = $(events.target).find("input[name=" + data["messages"][i]["name"] +"]");
                targetField.closest(".field-group").addClass("has-danger");
                targetField.tooltip({
                    delay: {
                        "show":500,
                        "hide":100
                    },
                    placement: 'top',
                    title: data["messages"][i]["error"],
                    trigger: 'manual'
                });
                targetField.tooltip("show");
                targetField.on('input propertychange paste', function () {
                    $(this).tooltip('dispose');
                    targetField.closest(".field-group").removeClass("has-danger");
                });
            };
        };
    });
};

$(document).ready(function() {
    var enableSortable = function () {
        $(".sortable").sortable({
            forcePlaceholderSize: true
        }).bind('sortupdate', function () {
            $("ul.sortable.list li").each(function (index, item) {
                var fieldname = $(this).find("input[name=placement]").val(index);
            })
        });
    };
    var destroySortable = function () {
        $(".sortable").sortable("destroy");
    }
    enableSortable();
    $('#addfield').on('click', function() {
        var totalFields = $("ul.sortable.list li").length
        typeid = $("#addfield").data("id");
        $.ajax('/manage/fields/new/' + typeid).done(function(html) {
            destroySortable();
            $('#customtypefields').append(html);
            var newfield = $("ul.sortable.list li").last();
            newfield.find("input[name=placement]").val(totalFields);
            enableSortable();
        });
    });
    $('.deletefield').on('click', function() {
        var fieldid = $(this).data("fieldid");
        var modal = $("#deleteconfirm");
        var typeid = modal.data("typeid");
        var typename = modal.data("typename");
        var url = "/manage/" + typename + "/" + typeid + "/field/" + fieldid + "/delete";
        modal.find("#deletebutton").attr("href", url);
    });
    $('#customtypefields').on('click', '.deletenewfield', function() {
        $(this).closest('.row').remove()
    });
    $('#customtypefields').on('submit', '.customfield', function(events) {
        var csrftoken = $(events.target).find("input[name=csrf_token]").val();
        var formData = $(events.target).serialize();
        submitAndVerifyForm(events, formData, '/manage/fields/edit', csrftoken);
        event.preventDefault();
        return false;
    });
    $('#customtypefields').on('submit', '.newcustomfield', function(events) {
        var csrftoken = $(events.target).find("input[name=csrf_token]").val();
        var typeid = $(events.target).find("input[name=typeid]").val()
        var formData = $(events.target).serialize();
        submitAndVerifyForm(events, formData, '/manage/fields/new/' + typeid, csrftoken, true);
        event.preventDefault();
        return false;
    });
    $('#saveall').on('click', function(events) {
        $('.customfield').each(function() {
            $(this).trigger('submit');
        });
        $('.newcustomfield').each(function() {
            $(this).trigger('submit');
        });
    });
} );

