from os import environ
from flask import Flask
from flask_assets import Environment, Bundle
from flask.ext.security import Security, MongoEngineUserDatastore
from flask_debugtoolbar import DebugToolbarExtension
from extracker.database import *

app = Flask(__name__)
app.config.from_pyfile('default.cfg')
if "ENVIRONMENT" in environ:
    if environ["ENVIRONMENT"] == "PRODUCTION":
        app.config.from_pyfile('production.cfg')
else:
    app.config.from_pyfile('development.cfg')
db.init_app(app)
toolbar = DebugToolbarExtension(app)
# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)

assets = Environment(app)

js = Bundle(
        'js/jquery.min.js',
        'js/tether.min.js',
        'js/bootstrap.min.js',
        'js/jquery.dataTables.min.js',
        'js/dataTables.bootstrap4.min.js',
        'js/jquery.sortable.min.js',
        'js/global.js',
        filters='jsmin',
        output='gen/packed.js'
        )
css = Bundle(
        'css/dataTables.bootstrap4.min.css', 
        'css/tether.min.css', 
        'css/tether-theme-basic.min.css', 
        'css/bootstrap.css', 
        'css/font-awesome.min.css', 
        'css/site.css', 
        filters='cssmin', 
        output='gen/packed.css'
        )
entryview = Bundle('js/entryview.js', filters='jsmin', output='gen/page.js')
managetype = Bundle('js/managetype.js', filters='jsmin', output='gen/page.js')
users = Bundle('js/users.js', filters='jsmin', output='gen/page.js')
assets.register('js_all', js)
assets.register('css_all', css)
assets.register('entryview', entryview)
assets.register('managetype', managetype)
assets.register('users', users)


## Seed data for dev
@app.before_first_request
def seedDatabase():
    try:
        if app.config["TEST"]:
            db.connection.drop_database(app.config["MONGODB_SETTINGS"]["db"])
            fwfields = [ 
                    {"name": "SRC IP", "type":"String", "required": True},
                    {"name": "SRC Host", "type":"String", "required": False},
                    {"name": "DST IP", "type":"String", "required": True},
                    {"name": "DST Host", "type":"String", "required": False}
                ]
            lafields = [
                    {"name": "User Name", "type":"String", "required": True},
                    {"name": "Host Name", "type":"String", "required": True}
                    ]
            t0 = ExceptionType(typename="Firewall", owner="Luke Snyder", description="Exceptions to our egress firewall policy.").save()
            for each in fwfields:
                ef = Field(
                        fieldname=each["name"],
                        fieldfield=''.join(e for e in each["name"] if e.isalnum()).lower(),
                        fieldrequired=each["required"],
                        fieldtype=each["type"]
                        )
                t0.update(add_to_set__fields=ef)
            t1 = ExceptionType(typename="Local Admin", owner="Bill Gates", description="Exceptions to our local admin policy").save()
            for each in lafields:
                ef = Field(
                        fieldname=each["name"],
                        fieldfield=''.join(e for e in each["name"] if e.isalnum()).lower(),
                        fieldrequired=each["required"],
                        fieldtype=each["type"]
                        )
                t1.update(add_to_set__fields=ef)
            e0 = ExceptionEntry(
                    exceptiontype=t0,
                    srcip="10.0.0.1",
                    srchost="",
                    dstip="8.8.8.8",
                    dsthost=""
            ).save()
            e1 = ExceptionEntry(
                    exceptiontype=t0,
                    srcip="10.1.1.1",
                    srchost="",
                    dsthost="google.com",
                    dstip=""
            ).save()
            e2 = ExceptionEntry(
                    exceptiontype=t1,
                    username="doris",
                    hostname="testPC"
            ).save()
    except KeyError:
        pass
    if User.objects.count() == 0:
        user_datastore.find_or_create_role(name="admin", description="Admin Role")
        user_datastore.find_or_create_role(name="reader", description="Reader Role")
        user_datastore.create_user(email="admin@admin.com", password="admin")
        user_datastore.add_role_to_user('admin@admin.com', 'admin')

import extracker.views

