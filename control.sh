#$!/bin/bash
HOME='<path to exception tracker>'
VIRTUALENVNAME='<virtualenv name (top level folder)>'
## Environment setup
# ENVIRONMENT='PRODUCTION'
ENVIRONMENT='DEVELOPMENT'

## DON'T EDIT BELOW THIS LINE

start () {
        cd $HOME
        source $VIRTUALENVNAME/bin/activate
        nohup sh -c "ENVIRONMENT='$ENVIRONMENT' python runserver.py -H 0.0.0.0 -p 8080" &
        PID=$!
        echo -n $PID > /tmp/exception-tracker.pid
        echo "Exception tracker started"
}

stop () {
        kill -9 `cat /tmp/exception-tracker.pid`
        echo "Exception tracker stopped"
}

case $1 in
        start) start ;;
        stop) stop ;;
        *) echo "$0 [start|stop]" ;;
esac