Track any kind of exception to security policy

# Installation
More detailed instructions to come

``` sh
git clone https://gitlab.com/lksnyder0/exception-tracker.git
cd exception-tracker
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Development
``` sh
cp exception-tracker/development.conf.example exception-tracker/development.conf
python runserver.py
```

## Production
``` sh
cp exception-tracker/production.conf.example exception-tracker/production.conf
ENVIRONMENT="PRODUCTION" python runserver.py
```
